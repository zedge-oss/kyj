package main

import (
	"flag"
	"fmt"
	"github.com/ghodss/yaml"
	_ "github.com/santhosh-tekuri/jsonschema/v2/httploader"
	"io/ioutil"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	isJson := flag.Bool("json", false, "Convert filename from YAML to JSON")
	isYaml := flag.Bool("yaml", false, "Convert filename from JSON to YAML")

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage of %s:\n <filename> [filename] [filename]\n", os.Args[0])

		flag.PrintDefaults()
	}
	flag.Parse()
	args := flag.Args()

	if *isJson == *isYaml {
		flag.Usage()
		os.Exit(1)
	}

	firstDocument := true
	for _, filename := range args {
		if !firstDocument {
			fmt.Println("---")
		}
		var data []byte
		var err error
		if filename == "-" {
			data, err = ioutil.ReadAll(os.Stdin)
			check(err)
		} else {
			data, err = ioutil.ReadFile(filename)
			check(err)
		}

		var parsedData []byte
		if *isJson {
			parsedData, err = yaml.YAMLToJSON(data)
			check(err)
		} else if *isYaml {
			parsedData, err = yaml.JSONToYAML(data)
			check(err)
		}
		fmt.Println(string(parsedData))
		firstDocument = false
	}
}
