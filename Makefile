all: kyj yj


# Build manager binary
kyj: fmt vet
	go build -o bin/kyj gitlab.com/zedge-oss/kyj/cmd/kyj

yj: fmt vet
	go build -o bin/yj gitlab.com/zedge-oss/kyj/cmd/yj

# Run go fmt against code
fmt:
	go fmt ./cmd/...

# Run go vet against code
vet:
	go vet ./cmd/...
