# KYJ  / Kubernetes YAML JsonSchema (Validator)

Simply tool which can be used against kubernetes (custom) resources to validate em against a jsonschema. 

Useful for developers making CRDs (custom resource definitions) and want to validate CRs (custom resources) before applying em to a kubernetes cluster.

This tool simply builds on top of the great work done by https://github.com/santhosh-tekuri/jsonschema .

## Usage

```
./bin/kyj -schema experiment-spec-v1alpha1.json examples-yaml/*

"examples-yaml/1err.yaml" does not conform to the schema specified. reason:
I[#] S[#] doesn't validate with "experiment-spec-v1alpha1.json#"
  I[#/serverTrigger] S[#/properties/serverTrigger/additionalProperties] additionalProperties "dogFoodIp" not allowed
---
"examples-yaml/3err.yaml" does not conform to the schema specified. reason:
I[#] S[#] doesn't validate with "experiment-spec-v1alpha1.json#"
  I[#/serverTrigger] S[#/properties/serverTrigger/additionalProperties] additionalProperties "dogFoodIp" not allowed
```

## How to build

```bash
make
```

## Alternative

```
./bin/yj -json examples-yaml/2ok.yaml  | jq .spec > experiment.json
./jv experiment-spec-v1alpha1.json experiment.json
```

Would work as well if you use a new enough version (see
https://github.com/santhosh-tekuri/jsonschema/issues/21 ), but intend to get
more first class support for directly eating kubernetes (custom) resources and
crds for schema in `kyj`.
